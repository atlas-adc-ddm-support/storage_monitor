Storage monitor
===============

This script generates several tables and plots, grouped by various combinations, eg T0, T1, T2, T3 and DATADISK, PRODDISK, SCRATCHDISK.

The tables are available at http://adc-ddm-mon.cern.ch/ddmusr01/

To fix strange values in the plots use the fix-rrd-fluctuations.py script. See the instructions inside to adjust the script according to the particular problem site, value, time etc.

