#!/bin/sh

#f=/var/www/html/ddmusr01/plots/AGLT2_DATADISK.rrd 
influx_host=`awk '/host = / {print $3}' /etc/adc_influxdb.conf`
influx_port=`awk '/port = / {print $3}' /etc/adc_influxdb.conf`
influx_user=`awk '/user = / {print $3}' /etc/adc_influxdb.conf`
influx_db=`awk '/db = / {print $3}' /etc/adc_influxdb.conf`
influx_pass=`awk '/pass = / {print $3}' /etc/adc_influxdb.conf`

for f in /var/www/html/ddmusr01/plots/*.rrd; do
  short_f=$(basename $f)
  site=${short_f%%.*}
  echo $site
  date
  :>${site}.curlin
  rrdtool dump $f | sed -n '/<database>/,/database>/p' | grep '<!--' | grep -v 'NaN' | awk '{printf("srmtotal=%s,rucioused=%s,usedothers=%s,quotaothers=%s,dark=%s,srmfree=%s,unlocked=%s,limit=%s,difference=%s %s\n",$9,$11,$13,$15,$17,$19,$21,$23,$25,$6)}' | while read values timestamp; do
    echo "site_occupancy,site=${site} ${values} ${timestamp}000000000" >>${site}.curlin
done
    curl -s -i -XPOST "https://${influx_host}:${influx_port}/write?db=${influx_db}&u=${influx_user}&p=${influx_pass}" --data-binary @${site}.curlin
    rm ${site}.curlin
done
